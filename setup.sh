#!/bin/bash
if [ "$(hostname)" = "fry" ]
then
    echo "Starting setup for Fry..."
    if [ ! -e ~/.bashrc ]
    then
        ln -s $(pwd)/bashrc-fry ~/.bashrc
        echo "~/.bashrc linked"
        source ~/.bashrc
    fi
else
    echo "Starting setup for local..."
    if [ ! -e ~/.bash_profile ]
    then
        ln -s $(pwd)/bash_profile ~/.bash_profile
        echo "~/.bash_profile linked"
        source ~/.bash_profile
    fi
fi

# Make directories for .vim/colors
if [ ! -d ~/.vim ]
then 
    echo "~/.vim does not exist--making directory"
    mkdir ~/.vim
else
    if [ ! -d ~/.vim/colors ]
    then
        echo "~/.vim/colors does not exist--making directory"
        mkdir ~/.vim/colors
    fi
fi

# Make a list of all the files where we can do the same action
SKIP=( "bash_profile" "bashrc-fry" "setup.sh" "ir_black.vim" "tmp_all" "tmp_skip" )
ALL=$(ls)
printf '%s\n' "${ALL[@]}" | sort >tmp_all
printf '%s\n' "${SKIP[@]}" | sort >tmp_skip
FILES=$(comm -23 tmp_all tmp_skip)

for FILE in $FILES
do
    if [ ! -e ~/.$FILE ]
    then
        ln -s $(pwd)/$FILE ~/.$FILE
        echo "~/.$FILE linked"
    fi
done

if [ ! -e ~/.vim/colors/ir_black.vim ]
then
    ln -s $(pwd)/ir_black.vim ~/.vim/colors/ir_black.vim
fi

rm tmp_skip tmp_all
echo "Setup complete"
