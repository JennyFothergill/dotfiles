alias ll='ls -l'
alias la='ls -la'
alias vmd='rlwrap /Applications/VMD\ 1.9.4.app/Contents/MacOS/startup.command'
alias fresh='clear;ls'
alias vpnon="/opt/cisco/anyconnect/bin/vpn connect bsuvpn-oncampus.boisestate.edu"
alias vpnoff="/opt/cisco/anyconnect/bin/vpn connect bsuvpn-offcampus.boisestate.edu"
alias vpndc="/opt/cisco/anyconnect/bin/vpn disconnect"
alias ip="curl ifconfig.me; echo"

function killport() {
    # kills the process listening to that port
    # usage: killport <port>
    # default is 8889
    if [ $# -eq 0 ]; then
        echo "killing process listening to port 8889";
        kill -9 $(sudo lsof -ti tcp:8889)
    else
        if [ $# -lt 2 ]; then
            echo "killing process listening to port $1"
            kill -9 $(sudo lsof -ti tcp:$1)
        fi
    fi
}

function firefox() {
    # opens the file in firefox
    # useful for gifs
    /Applications/Firefox.app/Contents/MacOS/firefox $(pwd)/$1
}

function frynb() {
    # open ssh channel to fry so notebook can be viewed in local browser
    # usage: frynb <port>
    # default port is 8889
    if [ $# -eq 0 ]; then
        echo "using port 8889";
        ssh -N -f -L localhost:8889:localhost:8889 jennyfothergill@fry;
    else
        if [ $# -lt 2 ]; then
            echo "using port $1"
            ssh -N -f -L localhost:$1:localhost:$1 jennyfothergill@fry;
        fi
    fi
}

function redhawknb() {
    # open ssh channel to redhawk so notebook can be viewed in local browser
    # usage: frynb <port>
    # default port is 8889
    if [ $# -eq 0 ]; then
        echo "using port 8889";
        ssh -N -f -L localhost:8889:localhost:8889 jennyfothergill@redhawk;
    else
        if [ $# -lt 2 ]; then
            echo "using port $1"
            ssh -N -f -L localhost:$1:localhost:$1 jennyfothergill@redhawk;
        fi
    fi
}

function catty() {
    # like cat but with line numbers
    # usage: catty file startnumber stopnumber
    head -$3 $1 |tail -$(expr $3 - $2 + 1)
}


#bind 'TAB':menu-complete
#bind "set show-all-if-ambiguous on"
#bind "set menu-complete-display-prefix on"

export PS1="\h:\W\$ "
export CLICOLOR=1
export LSCOLORS="gxfxcxdxbxegedabagacad"
export PATH=/Users/jennyfothergill/Library/Orca401:$PATH
export PATH=/Users/$(whoami)/Projects/Cassandra_V1.2/bin:${PATH}


# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
export CONDA=/Users/$(whoami)/miniconda3/bin/conda
export CONDA_PATH=/Users/$(whoami)/miniconda3
__conda_setup="$('$CONDA' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "$CONDA_PATH/etc/profile.d/conda.sh" ]; then
        . "$CONDA_PATH/etc/profile.d/conda.sh"
    else
        export PATH="$CONDA_PATH/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

conda activate
export PATH="/usr/local/opt/qt/bin:$PATH"

# Put .colors.csv in your home directory
# Add the following functions to your .bash_profile
#
# Change colors and fonts of the OSX terminal from the command line:
# 
# $ set_foreground_color lime green
# $ set_font "Oxygen Mono" 12
#
# Colors for Apple Terminal
#
function list_colors {
    cat ${HOME}/.colors.csv
}

function grep_apple_color {
    grep "$*" ${HOME}/.colors.csv
}

function get_apple_color {
    egrep "(^|,)$*(,|\t)" ${HOME}/.colors.csv | cut -f 6
}

function set_foreground_color {
    color=$(get_apple_color $*)
    if [ "$color" != "" ] ; then
        osascript -e "tell application \"Terminal\" to set normal text color of window 1 to ${color}"
    fi
}    

function set_background_color {
    color=$(get_apple_color $*)
    if [ "$color" != "" ] ; then
        osascript -e "tell application \"Terminal\" to set background color of window 1 to ${color}"
    fi
}    

function set_theme {
    set_foreground_color $1
    set_background_color $2
}    

function set_font {
    osascript -e "tell application \"Terminal\" to set the font name of window 1 to \"$1\""
    osascript -e "tell application \"Terminal\" to set the font size of window 1 to $2"
}

set_font Monaco 24
set_foreground_color cyan
set_background_color black
