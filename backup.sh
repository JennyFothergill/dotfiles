#!/bin/bash
COPY_FILES=( "POSCAR*" "CONTCAR" "KPOINTS" "OUTCAR" "*.o*" "*.pbs" "WAVCAR" "INCAR" "XDATCAR" )
COPY_FILES=( "setup.py" )
MYDIR=$1
DIRONLY=$( echo ${MYDIR##/*/} )
DIRPATH=${MYDIR%"$DIRONLY"}

#rclone mkdir google:$DIRONLY
echo "rclone mkdir google:$DIRONLY"

for FILE in ${COPY_FILES[@]}
do
    FOUND=`find $MYDIR -name "$FILE"`
    for FFILE in ${FOUND[@]}
    do
        path=$( echo ${FFILE%/*} )
        path=$( echo ${path#"$DIRPATH"} )
        file=$( echo ${FFILE##/*/} )
        if [ "$path" != "$DIRONLY" ]
        then
            echo "rclone mkdir google:$path"
            #rclone mkdir google:$path
        fi
        #rclone copy $FFILE google:$path
        echo "rclone copy $FFILE google:$path"
    done
done
