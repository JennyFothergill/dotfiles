" Install Plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)

call plug#begin('~/.vim/plugged')
" Syntax checking for every language
Plug 'scrooloose/syntastic'
" PEP8
Plug 'nvie/vim-flake8'
call plug#end()

"Use these comands to keep things updated
"PlugInstall
"PlugUpdate

" PEP8-ish
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=119 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set colorcolumn=80

" Find white space
highlight BadWhitespace ctermbg=red guibg=red
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" Pretty code
let python_highlight_all=1
syntax on
filetype on            " enables filetype detection
filetype plugin on     " enables filetype specific plugins

" Useful history settings
set history=700
set undolevels=700

" Remove trailing whitespace and new lines
autocmd BufWritePre *.py %s/\s\+$//e

" Syntastic Settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

" Run syntastic when F6 is pressed
let g:syntastic_mode="passive"
nnoremap <F6> :SyntasticCheck<CR>

"let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [],'passive_filetypes': [] }
"nnoremap <C-w>E :SyntasticCheck<CR> :SyntasticToggleMode<CR>

set clipboard=unnamed
set background=dark
colorscheme ir_black
syntax enable
set ts=4
set sw=4
set sts=4
set expandtab
set backspace=indent,eol,start
set ai
set ruler
set nobackup
set incsearch
set number
set cindent
hi Comment ctermfg=cyan

